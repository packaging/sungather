#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

script_dir="$(dirname "$(readlink -f "$0")")"
. "${script_dir}"/vars

cd "${clone}"

# fix imports
sed \
    -i \
    -e 's,"exports." + export,"SunGather.exports." + export,' \
    SunGather/sungather.py

/root/.pyenv/shims/pip3 install -r requirements.txt

/root/.pyenv/shims/nuitka3 \
    --onefile \
    --include-package=SunGather \
    --include-package=SunGather.exports \
    --remove-output \
    -o sungather \
    SunGather/sungather.py
mv sungather ../
cd -
rm -rf "${CI_PROJECT_DIR:-${PWD}}/${repo##*/}"
