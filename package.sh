#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

script_dir="$(dirname "$(readlink -f "$0")")"
. "${script_dir}"/vars

architecture="$(dpkg --print-architecture)"
description="Access ModBus data from almost any network connected Sungrow Inverter."

apt-get -qq update
apt-get -qqy install ruby-dev
fpm --help >/dev/null 2>&1 || gem install --quiet --no-document fpm

fpm \
    --force \
    --log "${fpm_loglevel:-error}" \
    --architecture "${architecture}" \
    --license "GPL-3.0" \
    --vendor "Bohdan Flower <github@bohdan.net>" \
    --input-type dir \
    --output-type deb \
    --package "sungather_${version}+${patchlevel}_${architecture}.deb" \
    --name sungather \
    --version "${version}+${patchlevel}" \
    --description "${description}" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "${repo}" \
    --depends 'sungather-data ( = '"${version}+${patchlevel}"' )' \
    --prefix /usr/bin \
    sungather

fpm \
    --force \
    --log "${fpm_loglevel:-error}" \
    --architecture all \
    --license "MIT" \
    --vendor "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --input-type dir \
    --output-type deb \
    --package "sungather-data_${version}+${patchlevel}_all.deb" \
    --name sungather-data \
    --version "${version}+${patchlevel}" \
    --description "data files for sungather" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "https://gitlab.com/packaging/sungather" \
    --prefix /usr/share/sungather/ \
    -C "${clone}" \
    SunGather/registers-sungrow.yaml

chmod o-w "${script_dir}"/.packaging/sungather-systemd/sungather@.service

fpm \
    --force \
    --log "${fpm_loglevel:-error}" \
    --architecture all \
    --license "MIT" \
    --vendor "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --input-type dir \
    --output-type deb \
    --package "sungather-systemd_${version}+${patchlevel}_all.deb" \
    --name sungather-systemd \
    --version "${version}+${patchlevel}" \
    --description "systemd unit for sungather" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "https://gitlab.com/packaging/sungather" \
    --before-install "${script_dir}/.packaging/sungather-systemd/before-install.sh" \
    --prefix /usr/lib/systemd/system/ \
    -C "${script_dir}"/.packaging/sungather-systemd \
    sungather@.service
