FROM debian:12-slim

RUN --mount=type=cache,target=/var/lib/apt/lists,id=debian-12-apt-lists,sharing=locked \
 --mount=type=cache,target=/var/cache/apt,id=debian-12-apt-cache,sharing=locked \
 apt-get update \
 && apt-get -y install --no-install-recommends ca-certificates

ADD --chown=_apt:root https://packaging.gitlab.io/sungather/gpg.key /etc/apt/trusted.gpg.d/morph027-sungather.asc

RUN echo "deb https://packaging.gitlab.io/sungather sungather main" >/etc/apt/sources.list.d/morph027-sungather.list

RUN adduser \
 --system \
 --shell /bin/bash \
 --gecos 'SunGather' \
 --group \
 --disabled-password \
 --home /var/lib/sungather \
 sungather

RUN --mount=type=cache,target=/var/lib/apt/lists,id=debian-12-apt-lists,sharing=locked \
 --mount=type=cache,target=/var/cache/apt,id=debian-12-apt-cache,sharing=locked \
 apt-get update \
 && apt-get -y install --no-install-recommends sungather

VOLUME /logs

VOLUME /config

USER sungather

CMD ["/usr/bin/sungather", "-r", "/usr/share/sungather/SunGather/registers-sungrow.yaml", "-c", "/config/config.yaml", "-l", "/logs/"]
