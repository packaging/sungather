#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

script_dir="$(dirname "$(readlink -f "$0")")"
. "${script_dir}"/vars

apt-get -qq update
apt-get -qqy install curl

curl -L https://gitlab.com/packaging/utils/-/raw/main/repo.sh | bash -s -- sungather
