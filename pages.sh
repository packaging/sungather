#!/bin/ash

set -e

mkdir -p "${CI_PROJECT_DIR}"/public

cp -r \
    "${CI_PROJECT_DIR}"/.repo/gpg.key \
    "${CI_PROJECT_DIR}"/.repo/sungather*/dists \
    "${CI_PROJECT_DIR}"/.repo/sungather*/pool \
    "${CI_PROJECT_DIR}"/public/
