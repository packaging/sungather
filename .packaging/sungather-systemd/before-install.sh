getent passwd sungather >/dev/null 2>&1 || adduser \
  --system \
  --shell /bin/bash \
  --gecos 'SunGather' \
  --group \
  --disabled-password \
  --home /var/lib/sungather \
  sungather

if [ ! -d /etc/sungather ]; then
  install -d -o sungather -g sungather -m 770 /etc/sungather
fi
