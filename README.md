[[_TOC_]]

# [SunGather](https://github.com/bohdan-s/SunGather) Packages

## What it is

* apt repo with packages
* statically compliled to binary using [Nuitka](https://github.com/Nuitka/Nuitka) - no python/virtualenv required

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).

## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-sungather.asc https://packaging.gitlab.io/sungather/gpg.key
```

## Add repo to apt

```bash
echo "deb https://packaging.gitlab.io/sungather sungather main" | sudo tee /etc/apt/sources.list.d/morph027-sungather.list
```

## Run as service using systemd

* install `sungather-systemd`
* create a config file `/etc/sungather/<name>.yaml` (where name can be anything e.g. _my-inverter_)
* create a file `/etc/default/sungather-<name>` and add `CONFIG=</path/to/config.yaml>`
* enable and start service: `systemctl enable --now sungather@<name>.service`

## Automatic updates using unattended-upgrades

```bash
echo 'Unattended-Upgrade::Allowed-Origins {"morph027:sungather";};' | sudo tee /etc/apt/apt.conf.d/50sungather
```
